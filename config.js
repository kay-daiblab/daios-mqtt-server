module.exports = {
    db: {
        mongodb_url: 'mongodb://35.200.115.136',
        mongodb_url_dev: 'mongodb://localhost:27017',
        db_name: 'daios'
    },
    mqtt: {
        url: 'mqtt://35.189.153.47',
        url_dev: 'mqtt://localhost',
    }
}
